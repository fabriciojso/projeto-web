<?php $pagina='categoria'; include "includes/header.php"; ?>
<div id="home">
   
    <h4>Lista de Produtos</h4>
    <div class="produtos">
        <div class="item">
            <a href="carrinho.php">
                <img src="img/relogio1.jpg" alt="imagem" />
                <h3>Relógio Orient Masculino Sport MBSSC162 P1SX</h3>
                <p class="preco"><span class="apenas">por apenas</span> R$ 695,00</p>
                <span class="comprar-bt">Comprar</span>
            </a>
        </div>
		<div class="item">
            <a href="carrinho.php">
                <img src="img/relogio2.jpg" alt="imagem" />
                <h3>Relógio Casio Feminino Vintage A168WG-9WDF</h3>
                <p class="preco"><span class="apenas">por apenas</span> R$ 349,00</p>
                <span class="comprar-bt">Comprar</span>
            </a>
        </div>
		<div class="item">
            <a href="carrinho.php">
                <img src="img/relogio3.jpg" alt="imagem" />
                <h3>Relógio Speedo Masculino 80579G0EVNP1</h3>
                <p class="preco"><span class="apenas">por apenas</span> R$ 99,00</p>
                <span class="comprar-bt">Comprar</span>
            </a>
        </div>
		<div class="item">
            <a href="carrinho.php">
                <img src="img/relogio4.jpg" alt="imagem" />
                <h3>Relógio Technos Masculino Performance JS26AB/8P</h3>
                <p class="preco"><span class="apenas">por apenas</span> R$ 399,00</p>
                <span class="comprar-bt">Comprar</span>
            </a>
        </div>
		<div class="item">
            <a href="carrinho.php">
                <img src="img/relogio5.jpg" alt="imagem" />
                <h3>Relógio Euro Feminino Opole EU2035LXM/4V</h3>
                <p class="preco"><span class="apenas">por apenas</span> R$ 199,00</p>
                <span class="comprar-bt">Comprar</span>
            </a>
        </div>
		<div class="item">
            <a href="carrinho.php">
                <img src="img/relogio6.jpg" alt="imagem" />
                <h3>Relógio Mondaine Feminino Troca Pulseira 76571LPMVDE3</h3>
                <p class="preco"><span class="apenas">por apenas</span> R$ 199,00</p>
                <span class="comprar-bt">Comprar</span>
            </a>
        </div>
		<div class="item">
            <a href="carrinho.php">
                <img src="img/relogio7.jpg" alt="imagem" />
                <h3>Relógio Euro Feminino Opole EU2035LXM/4D</h3>
                <p class="preco"><span class="apenas">por apenas</span> R$ 259,00</p>
                <span class="comprar-bt">Comprar</span>
            </a>
        </div>
		<div class="item">
            <a href="carrinho.php">
                <img src="img/relogio8.jpg" alt="imagem" />
                <h3>Relógio Technos Feminino Fashion Trend 2035LXU/4D</h3>
                <p class="preco"><span class="apenas">por apenas</span> R$ 329,00</p>
                <span class="comprar-bt">Comprar</span>
            </a>
        </div>
		<div class="item">
            <a href="carrinho.php">
                <img src="img/relogio9.jpg" alt="imagem" />
                <h3>Kit Relógio Mondaine Feminino com Brincos e Colar 83329LPMKDE1K1</h3>
                <p class="preco"><span class="apenas">por apenas</span> R$ 169,00</p>
                <span class="comprar-bt">Comprar</span>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php include "includes/footer.php"; ?>