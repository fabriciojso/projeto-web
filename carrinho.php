<?php $pagina='carrinho'; include "includes/header.php"; ?>
<div id="carrinho">
   	<div class="produto">
		
		<h2>Meu pedido:</h2>
		<br />
		<table width="100%" border="0" class="produtos">
			<thead>
				<td colspan="2">Produto</td>
				<td>Quantidade</td>
				<td>Valor unitário</td>
				<td>Valor total</td>
			</thead>
			<tr>
				<td><img src="img/relogio1.jpg" alt="imagem" width="40" height="40" /></td>
				<td>Relógio Orient Masculino Sport MBSSC162 P1SX</td>
				<td align="center"><input type="text" name="quantidade" value="1" readonly="readonly" /></td>
				<td align="center">R$ 695,00</td>
				<td align="center">R$ 695,00</td>
			</tr>
			<tr>
				<td><img src="img/relogio5.jpg" alt="imagem" width="40" height="40" /></td>
				<td>Relógio Euro Feminino Opole EU2035LXM/4V</td>
				<td align="center"><input type="text" name="quantidade" value="2" readonly="readonly" /></td>
				<td align="center">R$ 199,00</td>
				<td align="center">R$ 398,00</td>
			</tr>
			<tr>
				<td><img src="img/relogio8.jpg" alt="imagem" width="40" height="40" /></td>
				<td>Relógio Technos Feminino Fashion Trend 2035LXU/4D</td>
				<td align="center"><input type="text" name="quantidade" value="1" readonly="readonly" /></td>
				<td align="center">R$ 329,00</td>
				<td align="center">R$ 329,00</td>
			</tr>
			<tr class="total">
				<td colspan="4" align="right">Valor total &nbsp;&nbsp;</td>
				<td>&nbsp;&nbsp; R$ 1.422,00</td>
			</tr>
		</table>
		
		<br /><br />
		
		<div class="finalizar">
			<h3>Para o frete:</h3><br />
			<p><strong>CEP:</strong> <input type="text" maxlength="9" class="cep" />
			<!--<input type="button" class="calc_cep" value="OK" />-->
			</p>
			<br /><br /><br />
			<form action="index.php"><input type="submit" value="Finalizar compra" /></form>
		</div>
		
   	</div>
    <div class="clearfix"></div>
</div>
<?php include "includes/footer.php"; ?>