<?php $pagina='contato'; include "includes/header.php"; ?>
<div id="contato" class="page">
    <h1>Entre em contato</h1>
    <form method="post" action="index.php">
        <label>
            <span>Nome</span>
            <input type="text" name="nome" placeholder="Digite o seu nome" />
        </label>
         <label>
             <span>E-mail</span>
            <input type="text" name="email" placeholder="Digite o seu e-mail" />
        </label>
         <label>
             <span>Telefone</span>
            <input type="text" name="telefone" placeholder="Digite o seu telefone" />
        </label>
        <label>
            <span>Mensagem</span>
            <textarea name="mensagem" placeholder="Digite sua mensagem"></textarea>
        </label>
        <input type="submit" value="Enviar mensagem" />
    </form>
    <div class="clearfix"></div>
</div>
<?php include "includes/footer.php"; ?>