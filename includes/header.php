<?php
$ativo = array('', '', '', '');

if (isset($pagina)) {
	if ($pagina == 'categoria')
		$ativo[1] = ' class="active"';
	else if ($pagina == 'sobre')
		$ativo[2] = ' class="active"';
	else if ($pagina == 'contato')
		$ativo[3] = ' class="active"';
	else if ($pagina == 'carrinho')
		$ativo[0] = '';
	else
		$ativo[0] = ' class="active"';
} else {
	$ativo[0] = ' class="active"';
}
?>
<!DOCTYPE html>
<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Loja dos Relógios</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>
        <div id="header">
            <div class="center-content">
                <h1><a href="index.php"><img src="img/logo.png" alt="Loja dos Relógios" /><span>Loja dos Relógios</span></a></h1>
                <ul id="menu">
                    <li<?=$ativo[0];?>><a href="index.php">Início</a></li>
                    <li<?=$ativo[1];?>>Categorias
                        <ul class="submenu">
                            <li><a href="categoria.php">Aço</a></li>
                            <li><a href="categoria.php">Metal</a></li>
                            <li><a href="categoria.php">Couro</a></li>
                            <li><a href="categoria.php">Borracha</a></li>
                            <li><a href="categoria.php">Silicone</a></li>
                        </ul>
                    </li>
                    <li<?=$ativo[2];?>><a href="sobre.php">Sobre nós</a></li>
                    <li<?=$ativo[3];?>><a href="contato.php">Contato</a></li>
                </ul>
            </div>
        </div>
        <div id="site">
            <div id="conteudo">
                <form method="get" action="categoria.php" id="busca">
                    <input type="text" name="busca" placeholder="Digite aqui sua busca..." />
                    <input type="submit" name="enviar" value="Buscar">
                </form>
                <div class="center-content">